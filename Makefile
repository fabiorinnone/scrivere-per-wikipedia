# Makefile per articolo in LaTeX

default: all

all:
	make clean
	latex scrivere_per_wikipedia.tex
	latex scrivere_per_wikipedia.tex
	dvips -t landscape scrivere_per_wikipedia.dvi -o
	ps2pdf scrivere_per_wikipedia.ps
		
.tex:
	latex $@
	dvips -t a4 $?.dvi -o
	ps2pdf $@.ps
	
clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.loa
	rm -f *.lot
	rm -f *.lof
	rm -f *.idx
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.class
	rm -f *.bcf
	rm -f *.out
	rm -f *.run.xml
	rm -f *~

erase:
	make clean
	rm -f *.dvi*
	rm -f *.ps*
	rm -f *.pdf*

help:
	@echo "make [all]: genera i file .dvi e .pdf di tutti i file .tex"
	@echo "make scrivere_per_wikipedia: genera i file .dvi e .pdf " 
	@echo "make clean: elimina i file non necessari"
	@echo "make erase: elimina i file non necessari e i file .dvi, .ps e .pdf"
	@echo "Leggere anche README"
